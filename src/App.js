import React, { Component } from "react";
import bola from "./bola.png";
import "./App.css";

import * as Boot from "reactstrap";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jogador: "",
      jogadores: [],
      equipes: 2,
      jogaEquip: 5,
      times: null
    };
  }

  componentDidMount() {
    let cached = localStorage.getItem("_jogadores_")
    if (cached) {
      this.setState({ jogadores: [...JSON.parse(cached)]})
    }
  }

  digita(e) {
    this.setState({ jogador: e.target.value });
  }

  inclui() {
    if (this.state.jogador.trim() !== '') {
      this.state.jogadores.push({ nome: this.state.jogador });
      this.setState({
        jogador: "",
      });
    }
    let input = document.getElementById('edt_jogador')
    input.focus();
    localStorage.setItem("_jogadores_", JSON.stringify(this.state.jogadores))
  }

  enter(e) {
    if (e.keyCode === 13) {
      this.inclui()
    }
  }

  remove(i) {
    this.state.jogadores.splice(i, 1)
    this.setState({
      jogadores: this.state.jogadores
    })
  }

  number(f) {
    switch (f) {
      case 'inc':
        this.setState({
          equipes: this.state.equipes + 1
        })
        break;
      default:
        if (this.state.equipes > 2) {
          this.setState({
            equipes: this.state.equipes - 1
          })
        }
        break;
    }
  }

  handleRadio(val) {
    this.setState({
      jogaEquip: val
    })
  }

  clear() {
    this.setState({
      jogadores: []
    })
  }

  async sorteia() {
    let i = 0;
    let aux = [...this.state.jogadores]
    let times = []
    times.push([])
    do {
      let num = Math.floor(Math.random() * aux.length)

      times[i].push(aux[num])
      aux.splice(num, 1)

      if ((times[i].length >= this.state.jogaEquip) && (aux.length !== 0)) {
        times.push([])
        i++
      }
    } while (aux.length !== 0);
    this.setState({
      times
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={bola} className="App-logo" alt="bola" />
          <p>Sorteador</p>
        </header>
        <h3>Adicione os jogadores:</h3>
        <Boot.Container>
          <Boot.Row>
            <Boot.Col xl="4" lg="4" md="6" sm="6" xs="6">
              <Boot.Row>
                <span>Jogadores:</span>
                <Boot.InputGroup>
                  <Boot.Input id="edt_jogador" placeholder="Nome" value={this.state.jogador} onChange={e => this.digita(e)} onKeyDown={e => { this.enter(e) }} />
                  <Boot.InputGroupAddon addonType="append">
                    <Boot.Button
                      className="btn_incluir"
                      onClick={() => {
                        this.inclui();
                      }}
                    >
                      <span>Incluir</span>
                    </Boot.Button>
                  </Boot.InputGroupAddon>
                </Boot.InputGroup>
              </Boot.Row>
              <Boot.Row>
                <span>Jogadores por equipe:</span>
                <Boot.Container>
                  <Boot.Row>
                    <Boot.Col>
                      <Boot.Label>
                        <Boot.Input type="radio" checked={this.state.jogaEquip === 4} onChange={() => { this.handleRadio(4) }}></Boot.Input>
                        4 Pebas
                      </Boot.Label>
                    </Boot.Col>
                    <Boot.Col>
                      <Boot.Label>
                        <Boot.Input type="radio" checked={this.state.jogaEquip === 5} onChange={() => { this.handleRadio(5) }}></Boot.Input>
                        5 Pebas
                      </Boot.Label>
                    </Boot.Col>
                    <Boot.Col>
                      <Boot.Label>
                        <Boot.Input type="radio" checked={this.state.jogaEquip === 6} onChange={() => { this.handleRadio(6) }}></Boot.Input>
                        6 Pebas
                      </Boot.Label>
                    </Boot.Col>
                  </Boot.Row>
                </Boot.Container>
              </Boot.Row>
            </Boot.Col>
            {this.state.jogadores.length > 0 && (
              // <Boot.Row>
              <Boot.Col xl="4" lg="4" md="6" sm="6" xs="6">
                <Boot.ListGroup>
                  {this.state.jogadores.map((el, i) => (
                    <Boot.ListGroupItem key={i}>
                      <span className="nome">{el.nome}</span>&nbsp;
                      <Boot.Button
                        className="btn_pop"
                        onClick={() => { this.remove(i) }}>
                        <strong>X</strong>
                      </Boot.Button>
                    </Boot.ListGroupItem>
                  ))}
                </Boot.ListGroup>
                <Boot.Button
                  className="btn_sort"
                  onClick={() => { this.sorteia() }}>
                  Sortear!
                </Boot.Button>
                <Boot.Button
                  className="btn_clear"
                  onClick={() => { this.clear() }}>
                  Limpar!
                </Boot.Button>
              </Boot.Col>
              // </Boot.Row>
            )}
          </Boot.Row>
          {this.state.times && (
            <Boot.Row>
              {
                this.state.times.map((el, idx) => (
                  <Boot.Col xl="4" lg="4" md="6" sm="6" xs="6" key={idx}>
                    <Boot.ListGroup>
                      <Boot.ListGroupItem key={`time ${idx + 1}`}>
                        <span>Time &nbsp;<strong>{idx+1}</strong></span>
                      </Boot.ListGroupItem>
                      {
                        el.map((e, i) => (
                          <Boot.ListGroupItem key={`${e.nome}${i}`}>
                            <span><strong>{e.nome}</strong></span>
                          </Boot.ListGroupItem>
                        ))
                      }
                    </Boot.ListGroup>
                  </Boot.Col>
                ))
              }
            </Boot.Row>
          )}
        </Boot.Container>
      </div>
    );
  }
}

export default App;
